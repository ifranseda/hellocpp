#include "hello_world_impl.hpp"
#include <string>
#include <json/json.h>
#include <iostream>
#include "CategoryParser.hpp"

using namespace models;

namespace helloworld {
    
    shared_ptr<HelloWorld> HelloWorld::create() {
        return make_shared<HelloWorldImpl>();
    }
    
    HelloWorldImpl::HelloWorldImpl() {
    }
    
    string HelloWorldImpl::get_hello_world() {
        string myString = "Hello World! ";
        
        time_t t = time(0);
        tm now=*localtime(&t);
        char tmdescr[200]={0};
        const char fmt[]="%r";
        if (strftime(tmdescr, sizeof(tmdescr)-1, fmt, &now)>0) {
            myString += tmdescr;
        }
        
        return myString;
    }
    
    string HelloWorldImpl::api_call_implementation() {
        RestClient::init();
        
        // get a connection object
        RestClient::Connection* conn = new RestClient::Connection("http://api-electra.happyfresh.com/api");
        
        // set connection timeout to 5s
        conn->SetTimeout(30);
        
        // enable following of redirects (default is off)
        conn->FollowRedirects(true);
        
        // set headers
        RestClient::HeaderFields headers;
        headers["Accept"] = "application/json";
        headers["X-Spree-Client-Token"] = "electra-uwN4zRDAG4jq8S12fzbcbk0JggXVAnOMYmzTRHcb";
        conn->SetHeaders(headers);

        RestClient::Response r = conn->get("/taxonomies/1/taxons?stock_location_id=58&per_page=30");
        
        return r.body;
    }
    
    string HelloWorldImpl::sample_call() {
        string response = api_call_implementation().c_str();

        Json::Value root;
        Json::Reader reader;
        bool parsingSuccessful = reader.parse(response.c_str(), root);
        if (!parsingSuccessful) {
            return "ERROR : " + reader.getFormattedErrorMessages();
        }
        
        CategoryParser parser = CategoryParser();
        
        const Json::Value categories = root["taxons"];
        for (auto &cat : categories) {
            Category category = parser.parse(cat);
            std::cout << category.name << " ---- " << category.productCount << std::endl;
        }

        return "RESPONSE: ";// + response;
    }
   
}
