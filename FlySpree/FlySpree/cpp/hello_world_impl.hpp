#pragma once

#include "hello_world.hpp"
#include "restclient-cpp/connection.h"
#include "restclient-cpp/restclient.h"

using namespace RestClient;

namespace helloworld {
    
    class HelloWorldImpl : public helloworld::HelloWorld {
        
    private:
        string api_call_implementation();
        
    public:
        // Constructor
        HelloWorldImpl();
        
        // Our method that returns a string
        string get_hello_world();
        
        string sample_call();
    };
}
