//
//  main.cpp
//  FlySpree
//
//  Created by Isnan Franseda on 07/01/2017.
//  Copyright © 2017 Isnan Franseda. All rights reserved.
//

#include <iostream>
#include "hello_world_impl.hpp"

int main(int argc, const char * argv[]) {
    helloworld::HelloWorldImpl hw = helloworld::HelloWorldImpl();
    
    std::string myString = hw.get_hello_world();
    
    std::cout << myString << "\n";

    std::string sample = hw.sample_call();
    std::cout << sample << "\n";

    std::cout << "\n\nCOMPLETED!";

    return 0;
}
