//
//  CategoryParser.hpp
//  FlySpree
//
//  Created by Isnan Franseda on 10/01/2017.
//  Copyright © 2017 Isnan Franseda. All rights reserved.
//

#include <stdio.h>
#include "BaseParser.hpp"
#include "Category.hpp"

namespace models {
    
    class CategoryParser : public BaseParser<Category> {
    
    public:
        CategoryParser();

        static shared_ptr<CategoryParser> create();
        
        Category parse(Json::Value data);
    };
}
