//
//  CategoryParser.cpp
//  FlySpree
//
//  Created by Isnan Franseda on 10/01/2017.
//  Copyright © 2017 Isnan Franseda. All rights reserved.
//

#include "CategoryParser.hpp"
#include <list>
#include <iostream>

namespace models {

    CategoryParser::CategoryParser() {
    }
    
    shared_ptr<CategoryParser> CategoryParser::CategoryParser::create() {
        return make_shared<CategoryParser>();
    }

    Category CategoryParser::parse(const Json::Value data) {
        Category category;
        
        category.remoteId = data["id"].asInt64();
        category.name = data["name"].asString();
        category.prettyName = data["pretty_name"].asString();
        category.permalink = data["permalink"].asString();
        category.parentId = data["parent_id"].asInt64();
        category.taxonomyId = data["taxonomy_id"].asInt64();
        category.iconUrl = data["icon_url"].asString();
        category.productCount = data["products_count"].asInt();
        category.position = data["position"].asInt();
        category.categoryRanking = data["category_ranking"].asInt();

        const Json::Value subCats = data["taxons"];
        
        std::list<Category>::iterator it;
        it = category.subCategories.begin();

        for (auto &json_cat : subCats) {
            Category cat =  CategoryParser::parse(json_cat);
            category.subCategories.insert(it, cat);
            it++;
        }
        
        return category;
    }
}
