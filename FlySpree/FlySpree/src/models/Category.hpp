//
//  Category.hpp
//  FlySpree
//
//  Created by Isnan Franseda on 10/01/2017.
//  Copyright © 2017 Isnan Franseda. All rights reserved.
//

#pragma once

#include <memory>
#include <string>
#include <list>

namespace models {
    
    struct Category {
        int64_t remoteId;
        std::string name;
        std::string prettyName;
        std::string permalink;
        int64_t parentId;
        int64_t taxonomyId;
        std::string iconUrl;
        int productCount;
        int position;
        int categoryRanking;
        std::list<Category> subCategories;
        
        bool hasProduct() {
            return (productCount > 0) ? true : false;
        }
    };

}
