//
//  Parser.hpp
//  FlySpree
//
//  Created by Isnan Franseda on 10/01/2017.
//  Copyright © 2017 Isnan Franseda. All rights reserved.
//

#pragma once

#include <json/json.h>
#include <list>

namespace models {

    using namespace std;

    template <typename T>

class BaseParser {
public:
    virtual ~BaseParser() {};

    static shared_ptr<BaseParser> create();

    virtual T parse(Json::Value data) = 0;
};
}
